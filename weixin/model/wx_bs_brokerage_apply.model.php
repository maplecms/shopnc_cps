<?php
/**
 * Created by PhpStorm.
 * User: BesonTD
 * Date: 2015/3/28
 * Time: 9:53
 */
defined('BYshopJL') or exit('Access Invalid!');
//require_once(BASE_DATA_PATH.'/model/model.model.php');
class wx_bs_brokerage_applyModel extends Model{
    public function __construct(){
        parent::__construct('bs_brokerage_apply');
    }

    public function getList($condition, $order='apply_id desc'){
        $data_list = $this->table('bs_brokerage_apply,member')
            ->field('bs_brokerage_apply.*')
            ->join('left')->on('bs_brokerage_apply.member_id = member.member_id')
            ->where($condition)->order($order)->select();
        if (empty($data_list)) return array();
        return $data_list;
    }
}