<?php
/**
 * 商品管理
 *
 *
 *
 *
 * by www.besonit.com
 */
defined('BYshopJL') or exit('Access Invalid!');

class goods_viewModel extends Model
{
    public function __construct()
    {
        parent::__construct('bs_goods_view');
    }

    /**
     * updateGoodsView
     * 更新浏览记录
     * @param $goods_id
     * @return bool
     */
    public function updateGoodsView($goods_id)
    {
        if (empty($goods_id)) return false;
        $goods_model = Model('goods');
        $condition = array('goods_id' => $goods_id);

        $count = $goods_model->getGoodsCount($condition);
        if ($count == 0) return false;
        $year_month = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $view_count = $this->query("SELECT COUNT('goods_id') FROM {$this->table_prefix}bs_goods_view WHERE  `year_month`=$year_month AND `goods_id`=$goods_id");
        if (intval($view_count[0]["COUNT('goods_id')"]) == 0) {
            $sql = "INSERT INTO {$this->table_prefix}bs_goods_view  (`goods_id`,`year_month`, `month_view`,`d" . date('d') . "`) VALUES($goods_id,$year_month,1,1)";
        } else {
            $sql = "UPDATE {$this->table_prefix}bs_goods_view SET `d" . date('d') . "`= `d" . date('d') . "`+1 ,`month_view` = `month_view`+1 WHERE  `year_month`=$year_month AND `goods_id`=$goods_id";
        }
        $this->query($sql);
        return true;
    }
}
