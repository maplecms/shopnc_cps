<?php
/**
 * mobile公共方法
 *
 * 公共方法
 *
 * @package    function
 * @copyright  Copyright (c) 2007-2013 BesonIT Inc. (http://www.besonit.com)
 * @license    http://www.besonit.com/
 * @link       http://www.besonit.com/
 * @author	   ShopNC Team
 * @since      File available since Release v1.1
 */
defined('BYshopJL') or exit('Access Invalid!');

function output_data($datas, $extend_data = array(),$return = false) {
    $data = array();
    $data['code'] = 200;

    if(!empty($extend_data)) {
        $data = array_merge($data, $extend_data);
    }

    $data['datas'] = $datas;
    if($return){
        return json_encode($data);
    }

    if(!empty($_GET['callback'])) {
        echo $_GET['callback'].'('.json_encode($data).')';die;
    } else {
        echo json_encode($data);die;
    }
}

function output_error($message, $extend_data = array()) {
    $datas = array('error' => $message);
    output_data($datas, $extend_data);
}

/**
 * 成功消息
 * @param $code
 * @param $msg
 * @param $datas
 * Author: walkskyer
 */
function op_success($code,$msg,$datas=array()){
    op_data($code,$msg,$datas,'success');
}

/**
 * 失败消息
 * @param $code
 * @param $msg
 * @param array $datas
 * Author: walkskyer
 */
function op_error($code,$msg,$datas=array()){
    op_data($code,$msg,$datas,'error');
}

/**
 * 组装消息并发送
 * @param $code
 * @param string $msg
 * @param array $datas
 * @param string $type
 * @param bool $return
 * @return string
 * Author: walkskyer
 */
function op_data($code,$msg='',$datas=array(),$type='',$return=false){
    $data = array(
        'code'=>$code,
        'msg'=>$msg,
        'type'=>$type,
        );
    if($datas){
        $data['datas']=$datas;
    }
    if($return){
        return json_encode($data);
    }

    echo json_encode($data);die;
}

function mobile_page($page_count) {
    //输出是否有下一页
    $extend_data = array();
    $current_page = intval($_GET['curpage']);
    if($current_page <= 0) {
        $current_page = 1;
    }
    if($current_page >= $page_count) {
        $extend_data['hasmore'] = false;
    } else {
        $extend_data['hasmore'] = true;
    }
    $extend_data['page_total'] = $page_count;
    return $extend_data;
}





function record_message($data=array()){
    $model = Model('wx_message');
    $model->saveMessage($data);
}

function trimz($s) {
    $s=explode('.',$s);
    if (count($s)==2 && ($s[1]=rtrim($s[1],'0'))) return implode('.',$s);
    return $s[0];
}

