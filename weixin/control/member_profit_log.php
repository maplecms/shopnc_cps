<?php
/**
 * 我的地址
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2013 BesonIT Inc. (http://www.besonit.com)
 * @license    http://www.besonit.com
 * @link       http://www.besonit.com
 * @since      File available since Release v1.1
 */


defined('BYshopJL') or exit('Access Invalid!');

class member_profit_logControl extends wxMemberControl
{
    private $model_address;

    public function __construct()
    {
        parent::__construct();
        $this->model_address = Model('wx_address');
    }

    /**
     * 地址列表
     */
    public function data_listOp()
    {
        /* @var wx_profit_logModel $profit_log_model*/
        $profit_log_model = Model('wx_profit_log');
        $data_list = $profit_log_model->getProfitList(array('bs_profit_log.member_id' => $this->member_info['member_id']));
//        Tpl::output('address_list', $address_list);
//        Tpl::showpage('address_list', 'member_layout');
        output_data(array('data_list' => $data_list));
    }

    /**
     * 佣金账户设置
     * Author: walkskyer
     * Email:zwj_work@qq.com
     */
    public function brokerage_accountOp(){

    }

    /**
     * 佣金提现申请
     * Author: walkskyer
     * Email:zwj_work@qq.com
     */
    public function brokerage_applyOp(){

    }

    /**
     * 地区列表
     */
    public function area_listOp()
    {
        $area_id = intval($_POST['area_id']);

        $model_area = Model('area');

        $condition = array();
        if ($area_id > 0) {
            $condition['area_parent_id'] = $area_id;
        } else {
            $condition['area_deep'] = 1;
        }
        $area_list = $model_area->getAreaList($condition, 'area_id,area_name');
        output_data(array('area_list' => $area_list));
    }

}
