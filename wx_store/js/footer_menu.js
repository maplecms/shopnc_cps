$(function() {
    var tmpl = '<div class="widget_wrap">'
        +'<ul id="menu_4_ul" style="z-index:9999;">'
        +'<li><a href="/wx_store/index.html" data-fx="Modulefx"><span class="wicon-home"></span><p>首页</p></a></li>'
        +'<li><a href="/wx_store/tmpl/member/member.html?act=member" data-fx="Modulefx"><span class="wicon-info"></span><p>用户中心</p></a></li>'
        +'<li><a href="/wx_store/tmpl/cart_list.html" data-fx="Modulefx"><span class="wicon-cart"></span><p>购物车</p></a></li>'
        +'<li><a href="/wx_store/tmpl/product_first_categroy.html" data-fx="Modulefx"><span class="wicon-grid"></span><p>分类</p></a></li>'
        +'<li><a href="/wx_store/tmpl/search.html" data-fx="Modulefx"><span><i class="fa fa-search "></i></span><p>搜索</p></a></li>'
        +'</ul>'
        +'</div>';
    var html = template.compile(tmpl);
    $("#widget_wrap").html(html);
    bind4ChangHref();
    var menu_4 = (function () {
        var isTouch = "ontouchstart" in window;
        var menu = {
            handleEvent: function (evt) {
                console.log(evt);
            }
        };
        menu.init = function (id) {
            var ul = document.getElementById(id);
            lis = ul.querySelectorAll("li");
            lis[Math.floor(lis.length / 2)].classList.add("li3");
            if (isTouch) {
                menu.ul = document.getElementById(id);
                menu.ul.addEventListener("touchstart", menu, false);
                return menu;
            }
        }
        return menu;
    })().init("menu_4_ul");
});
