<?php
/**
 * 我的地址
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2013 BesonIT Inc. (http://www.besonit.com)
 * @license    http://www.besonit.com
 * @link       http://www.besonit.com
 * @since      File available since Release v1.1
 */


defined('BYshopJL') or exit('Access Invalid!');

class member_brokerageControl extends wxMemberControl
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 申请日志列表
     */
    public function apply_listOp()
    {
        /* @var wx_bs_brokerage_applyModel $bk_apply_model */
        $bk_apply_model = Model('wx_bs_brokerage_apply');
        $data_list = $bk_apply_model->getList(array('bs_brokerage_apply.member_id' => $this->member_info['member_id']));
        output_data(array('data_list' => $data_list));
    }
    /**
     * 佣金获取列表
     * Author: walkskyer
     * Email:zwj_work@qq.com
     */
    public function brokerage_listOp()
    {
        /* @var wx_bs_profit_logModel $bk_log_model */
        $bk_log_model = Model('wx_bs_profit_log');
        $data_list = $bk_log_model->getList("bs_profit_log.member_id = {$this->member_info['member_id']} AND bs_profit_log.`type`=1 AND bs_profit_log.amount >0");
        output_data(array('data_list' => $data_list));
    }

    /**
     * 查看佣金概况
     * Author: walkskyer
     * Email:zwj_work@qq.com
     */
    public function brokerage_viewOp()
    {
        /* @var wx_bs_brokerageModel $bk_model */
        $bk_model = Model('wx_bs_brokerage');
        $bk_data = $bk_model->getBrokerage("bk_member_id = {$this->member_info['member_id']}");
        if(!$bk_data){
            $bk_data=json_decode('{"bk_id":"0",
            "bk_member_id":"'.$this->member_info['member_id'].'",
            "bk_member_name":"","bk_total":"0.00",
            "bk_amount":"0.00","bk_blocking_apply":"0.00","bk_anticipated":"0.00",
            "bk_bank_name":"","bk_bank_no":"","bk_bank_user":"","bk_id_number":""}', true);
        }
        output_data(array('bk_data' => $bk_data));
    }

    /**
     * 佣金账户设置
     * Author: walkskyer
     * Email:zwj_work@qq.com
     */
    public function brokerage_accountOp(){
        $flag = false;
        /* @var wx_bs_brokerageModel $brokerage_model */
        $brokerage_model = Model('wx_bs_brokerage');
        if(!empty($_POST)) {
            $data = array(
                'bk_bank_no' => isset($_POST['bank_no']) && !empty($_POST['bank_no']) ? $_POST['bank_no'] : '',
                'bk_bank_user' => isset($_POST['bank_user']) && !empty($_POST['bank_user']) ? $_POST['bank_user'] : '',
                'bk_id_number' => isset($_POST['id_number']) && !empty($_POST['id_number']) ? $_POST['id_number'] : '',
            );

            $flag = $brokerage_model->save($data,array('bk_member_id'=>$_SESSION['member_id']));
        }else{
            $bk_value=$brokerage_model->where("bk_member_id={$_SESSION['member_id']}")->find();
            if($bk_value){
                op_data(1,'查询成功',$bk_value);
            }
            op_error(-1,'查询失败');
        }
        if($flag){
            op_success(1,'提现信息保存成功!',array('url'=>$_POST['url']));
        }
        op_error(-1,'提现信息保存失败');
    }

    /**
     * 佣金提现申请
     * Author: walkskyer
     * Email:zwj_work@qq.com
     */
    public function brokerage_applyOp(){

        $flag = false;
        if(!empty($_POST)) {
            $data = array(
                'apply_amount' => isset($_POST['apply_amount']) && !empty($_POST['apply_amount']) ? (float)$_POST['apply_amount'] : '',
            );
            if($data['apply_amount']<=0){
                output_data(array('msg'=>'请输入正确的提现金额，一个大于零的数字'));
            }
            $limit_amount=1;//余额提现限制
            /* @var wx_bs_brokerageModel $brokerage_model */
            $brokerage_model = Model('wx_bs_brokerage');
            $bk_value = $brokerage_model->where("bk_member_id={$_SESSION['member_id']}")->find();
            if(!$bk_value || $bk_value['bk_amount']<$limit_amount){
                output_data(array('msg'=>'您的现金余额未达到提现标准'));
            }
            if($data['apply_amount'] > $bk_value['bk_amount']){
                output_data(array('msg'=>'您申请的提现金额，超出现金余额，请修改。'));
            }
            if(!$bk_value['bk_bank_no'] || !$bk_value['bk_bank_user']){
                output_data(array('msg'=>'请完善您的提现账户信息'));
            }
            $bk_data=array('bk_amount'=>(float)($bk_value['bk_amount']-$data['apply_amount']),
                'bk_blocking_apply'=>(float)($bk_value['bk_blocking_apply']+$data['apply_amount']));
            $brokerage_model->update($bk_data,array('where'=>array('bk_member_id'=>$_SESSION['member_id'])));
            /* @var wx_bs_brokerage_applyModel $bk_log_model */
            $bk_apply_model = Model('wx_bs_brokerage_apply');
            $bk_apply_id = $bk_apply_model->insert(array(
                'member_id'=>$_SESSION['member_id'],
                'apply_time'=>time(),
                'amount'=>$data['apply_amount'],
                'bk_bank_name'=>$bk_value['bk_bank_name'],
                'bk_bank_no'=>$bk_value['bk_bank_no'],
                'bk_bank_user'=>$bk_value['bk_bank_user'],
                'modified'=>time(),
                'dateline'=>time(),
            ));
            if($bk_apply_id) {
                /* @var wx_bs_brokerage_apply_logModel $bk_log_model */
                $bk_apply_log_model = Model('wx_bs_brokerage_apply_log');
                $bk_apply_log_id = $bk_apply_log_model->insert(array(
                    'apply_id' => $bk_apply_id,
                    'op_id' => $_SESSION['member_id'],
                    'amount' => $data['apply_amount'],
                    'note' => '申请提现',
                    'dateline' => time()
                ));
                /* @var wx_bs_profit_logModel $profit_log_model */
                $profit_log_model = Model('wx_bs_profit_log');
                $profit_log_id = $profit_log_model->insert(array(
                    'member_id' => $_SESSION['member_id'],
                    'amount' => -$data['apply_amount'],
                    'note' => '申请提现',
                    'modified'=>time(),
                    'dateline'=>time(),
                ));
                $flag=$profit_log_id;
            }
        }
        if($flag){
            output_data(array('msg'=>'提现申请提交成功','url'=>$_POST['url']));
        }
        output_data(array('msg'=>'提现申请提交失败'));
    }
}
