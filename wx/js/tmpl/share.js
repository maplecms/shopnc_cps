var goods_id = GetQueryString("gid");


var share_title = '';
var share_imgUrl = '';
var share_desc = '点击领取红包';
var share_link = changeURLParam(get_root_url() + WapSiteUrl + '/share_out.html?gid=' + goods_id, 'rec', getcookie('share'));

$(function (){
    var unixTimeToDateString = function(ts, ex) {
        ts = parseFloat(ts) || 0;
        if (ts < 1) {
            return '';
        }
        var d = new Date();
        d.setTime(ts * 1e3);
        var s = '' + d.getFullYear() + '-' + (1 + d.getMonth()) + '-' + d.getDate();
        if (ex) {
            s += ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
        }
        return s;
    };

    var buyLimitation = function(a, b) {
        a = parseInt(a) || 0;
        b = parseInt(b) || 0;
        var r = 0;
        if (a > 0) {
            r = a;
        }
        if (b > 0 && r > 0 && b < r) {
            r = b;
        }
        return r;
    };

    template.helper('isEmpty', function(o) {
        for (var i in o) {
            return false;
        }
        return true;
    });


    //alert(goods_id);
    //渲染页面
    $.ajax({
       url:ApiUrl+"/index.php?act=goods&op=goods_detail",
       type:"get",
       data:{goods_id:goods_id,rec:GetQueryString('rec')},
       dataType:"json",
        async:false,
       success:function(result){

          var data = result.datas;
          if(!data.error){
            //商品图片格式化数据
              var goods_imgUrl;
            if(data.goods_image){
              var goods_image = data.goods_image.split(",");
              data.goods_image = goods_image;
                goods_imgUrl = data.goods_image[0];
            }else{
                goods_imgUrl = DefaultGoodsImg;
               data.goods_image = [];
            }

              if(data.goods_info.share_title!=''){
                  share_title=data.goods_info.share_title;
              }else{
                  share_title=data.goods_info.goods_name;
              }

              if(data.goods_info.share_note!=''){
                  share_desc=data.goods_info.share_note;
              }
              data.goods_info.goods_image_url = goods_imgUrl;
              //data.goods_info.pay_much = data.goods_info.goods_price - data.goods_info.profit;
              share_imgUrl = data.goods_info.goods_image_url;


            //商品规格格式化数据
            if(data.goods_info.spec_name){
              var goods_map_spec = $.map(data.goods_info.spec_name,function (v,i){
                var goods_specs = {};
                goods_specs["goods_spec_id"] = i;
                goods_specs['goods_spec_name']=v;
                if(data.goods_info.spec_value){
	                $.map(data.goods_info.spec_value,function(vv,vi){
	                    if(i == vi){
	                      goods_specs['goods_spec_value'] = $.map(vv,function (vvv,vvi){
	                        var specs_value = {};
	                        specs_value["specs_value_id"] = vvi;
	                        specs_value["specs_value_name"] = vvv;
	                        return specs_value;
	                      });
	                    }
	                  });
	                  return goods_specs;
                }else{
                	data.goods_info.spec_value = [];
                }
              });
              data.goods_map_spec = goods_map_spec;
            }else {
              data.goods_map_spec = [];
            }

            // 虚拟商品限购时间和数量
            if (data.goods_info.is_virtual == '1') {
                data.goods_info.virtual_indate_str = unixTimeToDateString(data.goods_info.virtual_indate, true);
                data.goods_info.buyLimitation = buyLimitation(data.goods_info.virtual_limit, data.goods_info.upper_limit);
            }

            // 预售发货时间
            if (data.goods_info.is_presell == '1') {
                data.goods_info.presell_deliverdate_str = unixTimeToDateString(data.goods_info.presell_deliverdate);
            }

            //渲染模板
            var html = template.render('product_detail', data);
            $("#product_detail_wp").html(html);

              share2out();
          }else {

            $.sDialog({
                content: data.error + '！<br>请返回上一页继续操作…',
                okBtn:false,
                cancelBtnText:'返回',
                cancelFn: function() { history.back(); }
            });

          }
       }
    });
  //点击商品规格，获取新的商品
});

function deal_share() {
    //未绑定手机
    if (getcookie('mobile_bind') != 1) {
        wx_redirect(WapSiteUrl + '/tmpl/member/binding.html');
        return false;
    }
    alert('点击右上角菜单分享');
}
