<link href="<?php echo SHOP_TEMPLATES_URL; ?>/css/member.css" type="text/css" rel="stylesheet">
<body>
<?php require_once template('common_header');?>
<div id="order-list" class="order-list-wp">
    <div class="order-list">
        <?php if($output['order_group_list']){?>
        <ul>
            <?php
                foreach($output['order_group_list'] as $k=>$v){
            ?>
            <li class="<?php if($v['pay_amount']){echo 'green-order-skin';}else{echo 'gray-order-skin';} if($k>0) echo 'mt10'; ?>">
                <div class="order-ltlt">
                    <p>
                        下单时间：
                        <?=date('Y-m-d H:i:s',$v['add_time']);?>
                    </p>
                </div>
                <?php
                    if($v['order_list']) {
                        foreach ($v['order_list'] as $key => $vv) {
                            ?>
                <div class="order-lcnt">
                    <div class="order-lcnt-shop">
                        <p>店铺名称：<?= $vv['store_name']; ?></p>

                        <p>订单编号：<?= $vv['order_sn']; ?></p>
                    </div>
                    <div class="order-shop-pd">
                        <?php
                            if ($vv['extend_order_goods']) {
                                foreach ($vv['extend_order_goods'] as $exk =>$extend) {
                        ?>
                            <a class="order-ldetail clearfix <?php if($exk>0) echo 'bd-t-de'; ?>"
                               href="<?=urlWx('goods','goods_detail',array('goods_id'=>$extend['goods_id']));?>">
                                            <span class="order-pdpic">
                                                <img src="<?=$extend['goods_image_url'];?>"/>
                                            </span>

                                <div class="order-pdinfor">
                                    <p><?=$extend['goods_name'];?></p>

                                    <p>
                                        单价：<span class="clr-d94">￥<?=$extend['goods_price'];?></span>
                                    </p>

                                    <p>
                                        商品数目：<?=$extend['goods_num'];?>
                                    </p>
                                </div>
                            </a>
                           <?php
                                 }
                            }
                           ?>
                    </div>
                    <div class="order-shop-total">
                        <p>运费：<span class="clr-d94">￥<?=$vv['shipping_fee'];?></span></p>

                        <p class="clr-c07">合计：￥<?=$vv['order_amount'];?> </p>

                        <p class="mt5">
                            <?php
                             $stateClass ="ot-finish";
                                $orderstate = $vv['order_state'];
                            if($orderstate == 20 || $orderstate == 30 || $orderstate == 40){
                                $stateClass = $stateClass;
                            }else if($orderstate == 0) {
                                $stateClass = "ot-cancel";
                            }else {
                                $stateClass = "ot-nofinish";
                            }
                            ?>
                            <span class="<?=$stateClass;?>"><?=$vv['state_desc'];?></span>
                        </p>

                        <p class="mt5">
                            <?php if($vv['if_receive']){?>
                            <a href="javascript:void(0)" order_id="<?=$vv['order_id'];?>"
                               class="sure-order">确认订单</a>
                            <?php }?>
                            <?php if($vv['if_cancel']){?>
                            <a href="javascript:void(0)" order_id="<?=$vv['order_id'];?>"
                               class="cancel-order">取消订单</a>
                            <?php }?>
                            <?php if($vv['if_deliver']){?>
                            <a href="javascript:void(0)" order_id="<?=$vv['order_id'];?>"
                               class="viewdelivery-order">查看物流</a>
                            <?php }?>
                        </p>
                    </div>
                </div>
                <?php
                        }
                    }

                    if($v['pay_amount'] && $v['pay_amount']>0){
                ?>

                <a href="<?=urlWx('member_payment','pay',array('pay_sn'=>$v['pay_sn']));?>" class="l-btn-login check-payment">订单支付（￥<?= $v['pay_amount']; ?>）</a>
                <?php }?>
            </li>
            <?php }?>
        </ul>
        <div class="pagination mt10">
            <a href="javascript:void(0);" class="pre-page <?php if($output['curpage'] <=1 ){echo 'disabled';}?>">上一页</a>
            <a href="javascript:void(0);" has_more="<?php if($output['pageSet']['hasmore']){echo 'true';}else{echo 'false';}?>"  class="next-page <?php if(!$output['pageSet']['hasmore']){echo 'disabled';}?>">下一页</a>
        </div>
        <?php }else {?>
        <div class="no-record">
            暂无记录
        </div>
        <?php }?>
    </div>
</div>
<?php require_once template('common_footer');?>
<script src="<?php echo SHOP_TEMPLATES_URL; ?>/js/simple-plugin.js" type="text/javascript"></script>
<script>
    $(function(){
        var curpage = <?=$output['curpage'] ;?>


        //取消订单
        $(".cancel-order").click(cancelOrder);
        //下一页
        $(".next-page").click(nextPage);
        //上一页
        $(".pre-page").click(prePage);

        //下一页
        function nextPage (){
            var self = $(this);
            var hasMore = self.attr("has_more");
            if(hasMore == "true"){
                curpage = curpage+1;
                window.location.href = ApiUrl+"/index.php?act=member_order&op=order_list&curpage="+curpage;
            }
        }
        //上一页
        function prePage (){
            var self = $(this);
            if(curpage >1){
                self.removeClass("disabled");
                curpage = curpage-1;
                window.location.href = ApiUrl+"/index.php?act=member_order&op=order_list&curpage="+curpage;
            }
        }

        //取消订单
        function cancelOrder(){
            var order_id = $(this).attr("order_id");
            $.sDialog({
                content: '确定取消订单？',
                okFn: function() { cancelOrderId(order_id); }
            });
        }

        function cancelOrderId(order_id) {
            $.ajax({
                type:"post",
                url:ApiUrl+"/index.php?act=member_order&op=order_cancel",
                data:{order_id:order_id},
                dataType:"json",
                success:function(result){
                    if(result.datas && result.datas == 1){
                        window.location.href = ApiUrl+"/index.php?act=member_order&op=order_list&curpage="+curpage;
                    }
                }
            });
        }
    });
</script>