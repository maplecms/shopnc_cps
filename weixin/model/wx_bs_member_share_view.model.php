<?php
/**
 * Created by PhpStorm.
 * User: BesonTD
 * Date: 2015/3/28
 * Time: 9:53
 */
defined('BYshopJL') or exit('Access Invalid!');
//require_once(BASE_DATA_PATH.'/model/model.model.php');
class wx_bs_member_share_viewModel extends Model
{
    public function __construct()
    {
        parent::__construct('bs_member_share_view');
        //$this->pk = 'bk_id';
    }

    public function get_share($condition)
    {
        $data = $this->where($condition)->find();
        if (empty($data)) return array();
        return $data;
    }

    /**
     * 获取一年的记录数
     * @param $condition
     * @return int
     * Author: walkskyer
     */
    public function get_year($condition)
    {
        $data = $this->where($condition)->order("`year_month` DESC")->limit(12)->sum('month_view');
        if (empty($data)) return 0;
        return $data;
    }
}