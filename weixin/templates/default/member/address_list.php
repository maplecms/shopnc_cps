<link href="<?php echo SHOP_TEMPLATES_URL; ?>/css/member.css" type="text/css" rel="stylesheet">
<body>
<?php require_once template('common_header');?>
<div id="address_list" class="address-list">
    <ul>
    <?php
        foreach($output['address_list'] as $v){
        ?>
        <li>
            <p class="madr-tlt clearfix">
                <span class="madrt-name"><?=$v['true_name']; ?></span>
                <span class="madrt-phone"><?=$v['mob_phone']; ?></span>
                <span class="madrt-type fright"></span>
            </p>
            <div class="madr-cnt">
                <p><?=$v['area_info']; ?>&nbsp;<?=$v['address']; ?></p>
                <p class="madrc-opera">
                    <a href="<?=urlWx('member_address','address_info',array('address_id'=>$v['address_id'])); ?>">编辑</a>&nbsp;|
                    <a class="deladdress" address_id="<?=$v['address_id']; ?>" href="javascript:;">删除</a>
                </p>
            </div>
        </li>
        <?php }?>


    </ul>
    <a href="<?=urlWx('member_address','address_info'); ?>" class="add_address mt10">添加新地址</a></div>
<?php require_once template('common_footer');?>
<script>
    $(function(){
        //点击删除地址
        $('.deladdress').click(delAddress);
        function delAddress(){
            var address_id = $(this).attr('address_id');
            $.ajax({
                type:'post',
                url:ApiUrl+"/index.php?act=member_address&op=address_del",
                data:{address_id:address_id},
                dataType:'json',
                success:function(result){
                    if(result){
                        window.location.reload();
                    }
                }
            });
        }
    })
</script>
