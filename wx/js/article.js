$(function() {
    var article_id = GetQueryString('article_id');
    if(!article_id){
        wx_redirect(HostUrl+WapSiteUrl);
    }else{
        $.ajax({
            url: ApiUrl + "/index.php?act=article",
            type: 'get',
            data:{id:article_id},
            dataType: 'json',
            success: function(result) {
                var data = result.datas;
                var html = '';

                html += template('article', data);
                html = HTMLDeCode(html);
                $("#main-container").html(html);
                $('title').html(data.article_title);
                var bs_info_width = $('.bs-info').width();
                $('.bs-info img').each(function(){
                    $(this).css('max-width',bs_info_width);
                });
                bind4ChangHref();
            }
        });
    }

    function    HTMLDeCode(str)
    {
        var    s    =    "";
        if    (str.length    ==    0)    return    "";
        s    =    str.replace(/&gt;/g,    "&");
        s    =    s.replace(/&lt;/g,        " <");
        s    =    s.replace(/&gt;/g,        ">");
        s    =    s.replace(/&nbsp;/g,        "    ");
        s    =    s.replace(/'/g,      "\'");
        s    =    s.replace(/&quot;/g,      "\"");
        s    =    s.replace(/ <br>/g,      "\n");
        return    s;
    }

});
