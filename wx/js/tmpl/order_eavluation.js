$(function(){
	var key = getcookie('key');
	if(key==''){
		logout(window.location.href);
	}
    $.ajax({
        type:'get',
        url:ApiUrl+"/index.php?act=member_evaluate&op=add&order_id="+GetQueryString('order_id'),
        data:{key:key},
        dataType:'json',
        success:function(result){
            checklogin(result.login);//检测是否登录了
            var data = result.datas;
            data.key = getcookie('key');
            data.WapSiteUrl = WapSiteUrl;//页面地址
            data.ApiUrl = ApiUrl;
            data.op = 'add';

            if(data.error){
                alert(data.error);
                wx_redirect(WapSiteUrl+'/tmpl/member/member.html');
            }

            template.helper('p2f', function(s) {
                return (parseFloat(s) || 0).toFixed(2);
            });
            var html = template.render('order-eavluation-tmpl', data);
            $("#order-eavluation").html(html);
        }
    });
});
