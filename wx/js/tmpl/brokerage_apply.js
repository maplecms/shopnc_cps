$(function(){
    var key = getcookie('key');
    //提交操作
    $('.wx_user_mobile_confirm').click(function(){
        var apply_amount = $('.wx_apply_amount').val();
        //var mobile_code = $('.wx_account').val();
        /*if(user_mobile == "" || user_mobile == undefined || user_mobile == null|| !reg.test(user_mobile)){
            $('#wx_msg').html('手机号不可为空或格式错误');
            $('#wx_msg').show();
            return false;
        }
        if(mobile_code == "" || mobile_code == undefined || mobile_code == null){
            $('#wx_msg').html('验证码不可为空');
            $('#wx_msg').show();
            return false;
        }*/

        //绑定手机
        $.ajax({
            type:'post',
            url:ApiUrl+'/index.php?act=member_brokerage&op=brokerage_apply',
            data:{'apply_amount':apply_amount,key:key},
            dataType:'json',
            success:function(result){
                if(result.datas.status==6){
                    $.sDialog({
                        skin:"block",
                        content:'提现申请提交成功',
                        okBtnText: "返回上一页",
                        cancelBtn: false,
                        okFn:function (){
                            wx_redirect('brokerage_view.html');
                        }
                    });
                    return false;
                }
                $('#wx_msg').html(result.datas.msg);
                $('#wx_msg').show();

            }
        });
    });


    //发送验证码
    $('.wx_user_send_code').click(function(){
        var user_mobile = $('.wx_user_mobile').val();
        var mobile_code = $('.wx_mobile_code').val();
        if(user_mobile == "" || user_mobile == undefined || user_mobile == null || !reg.test(user_mobile)){
            $('#wx_msg').html('手机号不可为空或格式错误');
            $('#wx_msg').show();
            return false;
        }

        if($('.wx_user_send_code').attr('st')==1) {
            //发送验证码操作
            $.ajax({
                type:'post',
                url:ApiUrl+'/index.php?act=member_index&op=sendMobileCode',
                data:{'mobile':user_mobile,key:key},
                dataType:'json',
                success:function(result){
                    if(result.datas.code==200){
                        $('#wx_msg').html(result.datas.msg);
                        $('#wx_msg').show();
                        return false;
                    }
                    $('#error-tips').html(result.datas.msg);
                    $('#error-tips').show();
                }
            });
        }


    });

    //隐藏消息提示
    $('input').click(function(){
        $('#wx_msg').hide();
    })

    var wait = 120;
    function time(o) {
        if (wait == 0) {
            o.html('重新获取验证码');
            o.attr('st',1);
            wait = 120;
        } else {
            o.html("发送中...(" + wait + ")");
            o.attr('st',2);
            wait--;
            setTimeout(function () {
                    time(o)
                },
                1000)
        }
    }
});