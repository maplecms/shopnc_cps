<?php
/**
 * Created by PhpStorm.
 * User: BesonTD
 * Date: 2015/3/28
 * Time: 9:53
 */
defined('BYshopJL') or exit('Access Invalid!');
require_once(BASE_DATA_PATH.'/model/goods.model.php');
class wx_goodsModel extends goodsModel{
    public function __construct($table = 'goods'){
        parent::__construct($table);
    }
}