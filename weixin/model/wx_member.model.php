<?php
/**
 * Created by PhpStorm.
 * User: BesonTD
 * Date: 2015/3/28
 * Time: 9:53
 */
defined('BYshopJL') or exit('Access Invalid!');
require_once(BASE_DATA_PATH . '/model/member.model.php');

class wx_memberModel extends memberModel
{
    /**
     * register
     * 注册会员
     * @param $register_info
     * @return array
     */
    public function register($register_info)
    {
        // 注册验证
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array("input" => $register_info["username"], "require" => "true", "message" => '用户名不能为空'),
            array("input" => $register_info["password"], "require" => "true", "message" => '密码不能为空'),
            array("input" => $register_info["password_confirm"], "require" => "true", "validator" => "Compare", "operator" => "==", "to" => $register_info["password"], "message" => '密码与确认密码不相同'),
            array("input" => $register_info["email"], "require" => "true", "validator" => "email", "message" => '电子邮件格式不正确'),
        );
        /*$error = $obj_validate->validate();
        if ($error != ''){
            return array('error' => $error);
        }*/

        if (!is_array($register_info) && empty($register_info)) return false;
        //判断是否存在此会员
        $condition['member_name'] = '__wx__' . $register_info['openid'];
        $memberInfo = $this->getMemberInfo($condition);
        if ($memberInfo['member_id']) {//存在此会员
            $this->createSession($memberInfo);
            return;
        }
        // 验证用户名是否重复
        $check_member_name = $this->getMemberInfo(array('member_name' => $register_info['username']));
        if (is_array($check_member_name) and count($check_member_name) > 0) {
            return array('error' => '用户名已存在');
        }

        // 验证邮箱是否重复
        $check_member_email = $this->getMemberInfo(array('member_email' => $register_info['email']));
        if (is_array($check_member_email) and count($check_member_email) > 0) {
            return array('error' => '邮箱已存在');
        }
        // 会员添加
        $member_info = array();
        $member_info['store_id'] = intval($_GET['store_id']) ? intval($_GET['store_id']) : 0;
        $member_info['member_name'] = '__wx__' . $register_info['openid'];
        $member_info['member_passwd'] = '__wx__123456';
        $member_info['member_email'] = '__wx__' . $register_info['openid'] . '@wx.com';
        $member_info['member_wxopenid'] = $register_info['openid'];
        $member_info['member_wxinfo'] = serialize($register_info);
        $member_info['member_wxunionid'] = isset($register_info['unionid']) ? $register_info['unionid'] : null;
        $member_info['share_sign'] = md5($register_info['openid']);
        //是否存在推荐
        if (isset($_GET['rec']) && !empty($_GET['rec'])) {
            $member = $this->infoMember(array('share_sign' => $_GET['rec']), 'member_id');
        }
        $member_info['recommend_id'] = isset($member) && $member['member_id'] ? $member['member_id'] : 0;
        $insert_id = $this->addMember($member_info);
        if ($insert_id) {
            //添加会员积分
            if (C('points_isuse')) {
                Model('points')->savePointsLog('regist', array('pl_memberid' => $insert_id, 'pl_membername' => $member_info['member_name']), false);
            }

            // 添加默认相册
            $insert['ac_name'] = '买家秀';
            $insert['member_id'] = $insert_id;
            $insert['ac_des'] = '买家秀默认相册';
            $insert['ac_sort'] = 1;
            $insert['is_default'] = 1;
            $insert['upload_time'] = TIMESTAMP;
            $this->table('sns_albumclass')->insert($insert);

            $member_info['member_id'] = $insert_id;
            $member_info['is_buy'] = 1;
            $this->createSession($member_info);
            return $member_info;
        } else {
            return array('error' => '注册失败');
        }

    }

    public function getMemberInfo($condition, $field = '*', $master = false)
    {
        return $this->table('member,bs_brokerage')->field($field)->join('left')->on('member.member_id=bs_brokerage.bk_member_id')->where($condition)->master($master)->find();
    }

    /**
     * 注册商城会员
     *
     * @param    array $param 会员信息
     * @return    array 数组格式的返回结果
     */
    public function addMember($param)
    {
        if (empty($param)) {
            return false;
        }
        try {
            $this->beginTransaction();
            $member_info = array();
            //$member_info['member_id'] = $param['member_id'];
            $member_info['member_name'] = $param['member_name'];
            $member_info['member_passwd'] = md5(trim($param['member_passwd']));
            $member_info['member_email'] = $param['member_email'];
            $member_info['member_time'] = TIMESTAMP;
            $member_info['member_login_time'] = TIMESTAMP;
            $member_info['member_old_login_time'] = TIMESTAMP;
            $member_info['member_login_ip'] = getIp();
            $member_info['member_old_login_ip'] = $member_info['member_login_ip'];

            $member_info['member_truename'] = $param['member_truename'];
            $member_info['member_qq'] = $param['member_qq'];
            $member_info['member_sex'] = $param['member_sex'];
            $member_info['member_avatar'] = $param['member_avatar'];
            $member_info['member_qqopenid'] = $param['member_qqopenid'];
            $member_info['member_qqinfo'] = $param['member_qqinfo'];
            $member_info['member_sinaopenid'] = $param['member_sinaopenid'];
            $member_info['member_sinainfo'] = $param['member_sinainfo'];

            $member_info['member_wxopenid'] = $param['member_wxopenid'];
            $member_info['member_wxinfo'] = $param['member_wxinfo'];
            $member_info['member_wxunionid'] = $param['member_wxunionid'];
            $member_info['share_sign'] = $param['share_sign'];
            $member_info['recommend_id'] = $param['recommend_id'];
            $insert_id = $this->table('member')->insert($member_info);
            if (!$insert_id) {
                throw new Exception();
            }
            $insert = $this->addMemberCommon(array('member_id' => $insert_id));
            if (!$insert) {
                throw new Exception();
            }
            $this->commit();
            return $insert_id;
        } catch (Exception $e) {
            $this->rollback();
            return false;
        }
    }


    /**
     * 登录时创建会话SESSION
     *
     * @param array $member_info 会员信息
     */
    public function createSession($member_info = array(), $reg = false)
    {
        if (empty($member_info) || !is_array($member_info)) return;
        if($_SESSION['is_login']) return ;

        $_SESSION['is_login'] = '1';
        $_SESSION['member_id'] = $member_info['member_id'];
        $_SESSION['member_name'] = $member_info['member_name'];
        $_SESSION['member_email'] = $member_info['member_email'];
        $_SESSION['is_buy'] = isset($member_info['is_buy']) ? $member_info['is_buy'] : 1;
        $_SESSION['avatar'] = $member_info['member_avatar'];

        $_SESSION['share_sign'] = $member_info['share_sign'];
        $_SESSION['member_wxopenid'] = $member_info['member_wxopenid'];
        $_SESSION['member_wxunionid'] = $member_info['member_wxunionid'];
        $_SESSION['member_store_id'] = $member_info['store_id'];

        $seller_info = Model('seller')->getSellerInfo(array('member_id' => $_SESSION['member_id']));
        $_SESSION['store_id'] = $seller_info['store_id'];

        if (trim($member_info['member_qqopenid'])) {
            $_SESSION['openid'] = $member_info['member_qqopenid'];
        }
        if (trim($member_info['member_sinaopenid'])) {
            $_SESSION['slast_key']['uid'] = $member_info['member_sinaopenid'];
        }

        if (!$reg) {
            //添加会员积分
            $this->addPoint($member_info);
            //添加会员经验值
            $this->addExppoint($member_info);
        }

        if (!empty($member_info['member_login_time'])) {
            $update_info = array(
                'member_login_num' => ($member_info['member_login_num'] + 1),
                'member_login_time' => TIMESTAMP,
                'member_old_login_time' => $member_info['member_login_time'],
                'member_login_ip' => getIp(),
                'member_old_login_ip' => $member_info['member_login_ip']
            );
            $this->editMember(array('member_id' => $member_info['member_id']), $update_info);
        }
        setNcCookie('cart_goods_num', '', -3600);

    }
}