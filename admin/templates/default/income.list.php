<?php defined('BYshopJL') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>收益记录</h3>
      <ul class="tab-base">
        <li><a href="JavaScript:void(0);" class="current"><span>收益统计</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form method="get" action="index.php" name="formSearch" id="formSearch">
    <input type="hidden" name="act" value="income" />
    <input type="hidden" name="op" value="index" />
    <input type="hidden" name="statistics" id="income_statistics" value="" />
    <table class="tb-type1 noborder search">
      <tbody>
        <tr>
          <th>会员</th>
          <td><input type="text" class="text" name="f_key" value="<?php echo trim($_GET['f_key']); ?>" placeholder="编号、手机号"/></td>
          <th><label for="add_time_from"><?php echo '起止日期';?></label></th>
          <td><input class="txt date" type="text" value="<?php echo $_GET['add_time_from'];?>" id="add_time_from" name="add_time_from">
            <label for="add_time_to">~</label>
            <input class="txt date" type="text" value="<?php echo $_GET['add_time_to'];?>" id="add_time_to" name="add_time_to"/></td>
          <td><a href="javascript:void(0);" id="ncsubmit" class="btn-search " title="<?php echo $lang['nc_query'];?>">&nbsp;</a></td>
        </tr>
      </tbody>
    </table>
  </form>
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td><ul>
            <li>会员收益统计</li>
            <li>为使查询信息更准确，请输入聊天双方的完整会员名——登录账号。</li>
            <li></li>
          </ul></td>
      </tr>
    </tbody>
  </table>
  <div class="chat-log">
          <table class="table tb-type2 nobdb">
              <thead>
              <tr class="thead">
                  <th class="align-center">会员编号</th>
                  <th class="align-center">手机号</th>
                  <?php if($_GET['statistics']){?>
                      <th class="align-center">订单数量</th>
                      <th class="align-center">收益金额</th>
                  <?php }else{ ?>
                      <th class="align-center">时间</th>
                      <th class="align-center">订单号</th>
                      <th class="align-center">订单金额</th>
                      <th class="align-center">收益金额</th>
                  <?php }?>
              </tr>
              <tbody>
              <?php if (is_array($output['income_list']) && !empty($output['income_list'])) { ?>
              <?php foreach ($output['income_list'] as $key => $val) { ?>
                      <tr class="hover member">
                          <td class="w100 align-center"><?php echo $val['member_id']; ?></td>
                          <td class="w150 align-center"><?php echo $val['member_mobile']; ?></td>
                          <?php if($_GET['statistics']){?>
                              <td class="align-center"><?php echo $val['all_order']; ?></td>
                              <td class="align-center"><?php echo $val['all_amount']; ?></td>
                          <?php }else{ ?>
                              <td class="w150 align-center"><?php echo date("Y-m-d H:i:s",$val['dateline']); ?></td>
                              <td class="align-center"><?php echo $val['order_sn']; ?></td>
                              <td class="align-center"><?php echo $val['order_amount']; ?></td>
                              <td class="align-center"><?php echo $val['amount']; ?></td>
                          <?php }?>
                      </tr>
                  <?php } ?>
              <?php }else { ?>
                  <tr class="no_data">
                      <td colspan="5"><?php echo $lang['nc_no_record']?></td>
                  </tr>
              <?php } ?>
              </tbody>
              <tfoot class="tfoot">
              <?php if(!empty($output['income_list']) && is_array($output['income_list'])){ ?>
                  <tr>
                      <td colspan="16">
                         <a href="JavaScript:void(0);" class="btn" onclick="$('#income_statistics').val(1);$('#formSearch').submit();"><span>统计记录</span></a>
                          &nbsp;&nbsp;&nbsp;
                          <a href="JavaScript:void(0);" class="btn" onclick="$('#income_statistics').val(0);$('#formSearch').submit();"><span>撤销统计</span></a>
                      </td>
                  </tr>
              <?php } ?>
              </tfoot>
          </table>
    <?php if (is_array($output['income_list']) && !empty($output['income_list'])) { ?>
    <div class="pagination"><?php echo $output['show_page']; ?></div>
    <?php } ?>
  </div>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/i18n/zh-CN.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<script type="text/javascript">
$(function(){
    $('#add_time_from').datepicker({dateFormat: 'yy-mm-dd'});
    $('#add_time_to').datepicker({dateFormat: 'yy-mm-dd'});
    $('#ncsubmit').click(function(){
    	$('#formSearch').submit();
    });
});
</script>
