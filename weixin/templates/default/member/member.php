<link href="<?php echo SHOP_TEMPLATES_URL; ?>/css/member.css" type="text/css" rel="stylesheet">

<body class="member-body">
    <?php require_once template('common_header');?>
<div class="m-top clearfix">
    <div class="m-avatar fleft">
        <img id="avatar" src="<?=$output['member_info']['avator']?>">
    </div>
    <div class="m-infor fleft">
        <div class="m-name mt10">
            昵称：<span id="username"><?=$output['member_info']['user_name']?></span>
        </div>
        <div class="clearfix mt10">
                <span class="m-jifen">
                    <span>积分</span>
                    <span id="point" class="clr-d94"><?=$output['member_info']['point']?></span>
                </span>
                 <span class="m-yue">
                      <span>预存款</span>
                      <span id="predepoit" class="clr-d94"><?=$output['member_info']['predepoit']?></span>
                 </span>
        </div>
    </div>
</div>
<div class="m-center">
    <ul class="mc-cnt">
        <li>
            <a href="<?=urlWx('member_order','order_list')?>">
                <i class="im-order"></i>
                我的订单
                <span class="grayrightarrow"></span>
            </a>
        </li>
        <li>
            <a href="vr_order_list.html">
                <i class="im-order"></i>
                我的虚拟订单
                <span class="grayrightarrow"></span>
            </a>
        </li>
        <li>
            <a href="voucher_list.html">
                <i class="im-quan"></i>
                我的代金券
                <span class="grayrightarrow"></span>
            </a>
        </li>
        <li>
            <a href="favorites.html">
                <i class="im-collect"></i>
                我的收藏
                <span class="grayrightarrow"></span>
            </a>
        </li>
        <li>
            <a href="<?=urlWx('member_address','address_list');?>">
                <i class="im-address"></i>
                地址管理
                <span class="grayrightarrow"></span>
            </a>
        </li>
        <li>
            <a href="views_list.html">
                <i class="im-history"></i>
                浏览历史
                <span class="grayrightarrow"></span>
            </a>
        </li>
    </ul>
</div>
<?php require_once template('common_footer');?>
