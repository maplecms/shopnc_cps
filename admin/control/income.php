<?php
/**
 * 收益记录查询
 *
 */

defined('BYshopJL') or exit('Access Invalid!');

class incomeControl extends SystemControl
{
    public function __construct()
    {
        parent::__construct();
        $add_time_to = date("Y-m-d");
        $time_from = array();
        $time_from['7'] = strtotime($add_time_to) - 60 * 60 * 24 * 7;
        if (empty($_GET['add_time_from']) || $_GET['add_time_from'] > $add_time_to) {//默认显示7天内数据
            $_GET['add_time_from'] = date("Y-m-d", $time_from['7']);
        }
        if (empty($_GET['add_time_to']) || $_GET['add_time_to'] > $add_time_to) {
            $_GET['add_time_to'] = $add_time_to;
        }
    }

    /**
     * 记录查询
     */
    public function indexOp()
    {


        $model_profit = Model('profit_log');
        $f_key = trim($_GET['f_key']);
        $condition = array();
        if (!empty($f_key)) {
            $condition['member.member_mobile|member.member_id'] = array(array('like', '%' . $f_key . '%'));
        }
        $condition['bs_profit_log.type'] = 1;
        $condition['bs_profit_log.dateline'] = array('between', intval(strtotime(trim($_GET['add_time_from']))) . ',' . intval(strtotime(trim($_GET['add_time_to']))));
        $income_list = $model_profit->getProfitList($condition);
        Tpl::output('income_list', $income_list);
        Tpl::output('show_page', $model_profit->showpage());
        Tpl::showpage('income.list');
    }
}
