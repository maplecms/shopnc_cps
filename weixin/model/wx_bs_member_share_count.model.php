<?php
/**
 * 商品管理
 *
 *
 *
 *
 * by www.besonit.com
 */
defined('BYshopJL') or exit('Access Invalid!');

class wx_bs_member_share_countModel extends Model
{
    public function __construct()
    {
        parent::__construct('bs_member_share_count');
    }

    public function get_count($condition)
    {
        $data = $this->where($condition)->find();
        if (empty($data)) return array();
        return $data;
    }

    /**
     * 获取一年的记录数
     * @param $condition
     * @return int
     * Author: walkskyer
     */
    public function get_year($condition)
    {
        $data = $this->where($condition)->order("`year_month` DESC")->limit(12)->sum('month_view');
        if (empty($data)) return 0;
        return $data;
    }

    /**
     * updateGoodsShareView
     * 更新分享浏览记录
     * @param $goods_id
     * @return bool
     */
    public function updateCount($member_id)
    {
        if (empty($member_id)) return false;

        $year_month = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $view_count = $this->where("`member_id`={$member_id} AND `year_month`={$year_month}")->find();
        if (!$view_count) {
            $flag =$this->insert(array('member_id'=>$member_id,'`year_month`'=>$year_month,'month_view'=>1,'d' . date('d')=>1));
        } else {
            $flag =$this->update(array('month_view'=>$view_count['month_view']+1,'d' . date('d')=>$view_count['d' . date('d')]+1),array('where'=>"`member_id`={$member_id} AND `year_month`={$year_month}"));
        }
        return true;
    }
}
