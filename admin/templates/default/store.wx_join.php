<?php defined('BYshopJL') or exit('Access Invalid!');?>

<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3><?php echo $lang['store'];?></h3>
      <ul class="tab-base">
        <li><a href="index.php?act=store&op=store"><span><?php echo $lang['manage'];?></span></a></li>
        <li><a href="index.php?act=store&op=store_joinin" ><span><?php echo $lang['pending'];?></span></a></li>
        <li><a href="index.php?act=store&op=reopen_list" ><span>续签申请</span></a></li>
        <li><a href="index.php?act=store&op=store_bind_class_applay_list" ><span>经营类目申请</span></a></li>
        <li><a href="JavaScript:void(0);"  class="current" ><span>微信申请入驻</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
   <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12"><div class="title">
            <h5><?php echo $lang['nc_prompts'];?></h5>
            <span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td><ul>
            <li><?php echo '联系客户，进行入驻操作';?></li>
          </ul>
        </td>
      </tr>
    </tbody>
  </table>
  <form method="post" id="store_form">
    <input type="hidden" name="form_submit" value="ok" />
    <table class="table tb-type2">
      <thead>
        <tr class="thead">
          <th>申请人姓名</th>
          <th>申请人手机号</th>
          <th>申请时间</th>
          <th>公司名称</th>
          <th>公司地址</th>
          <th>产品类型</th>
          <th class="align-center">状态</th>
          <th class="align-center">处理时间</th>
          <th class="align-center"><?php echo $lang['operation'];?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!empty($output['store_list']) && is_array($output['store_list'])){ ?>
        <?php foreach($output['store_list'] as $k => $v){ ?>
        <tr class="hover edit ">
          <td>
                <?php echo $v['username'];?>
          </td>
          <td><?php echo $v['tel'];?></td>
          <td><?php echo date('Y-m-d H:i:s', $v['dateline']);?></td>
          <td><?php echo $v['company'];?></td>
          <td><?php echo $v['address'];?></td>
          <td><?php echo $v['goods_type'];?></td>
          <td class="align-center"><?php if($v['status']==10){echo '未处理';}elseif($v['status']==20){echo '已处理';}?></td>
          <td class="nowarp align-center"><?php if($v['status']==20){echo date('Y-m-d H:i:s', $v['modified']);}else{echo '--';}?></td>
        <td class="align-center w200">
            <?php if($v['status']==10){?>
                <a href="javascript:void(0)" class="bs_join_apply" tel="<?php echo $v['tel']; ?>">登记</a>
            <?php }else{
                echo '---';
            }?>
            </td>
        </tr>
        <?php } ?>
        <?php }else { ?>
        <tr class="no_data">
          <td colspan="15"><?php echo $lang['nc_no_record'];?></td>
        </tr>
        <?php } ?>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td></td>
          <td colspan="16">
            <div class="pagination"><?php echo $output['page'];?></div></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.edit.js" charset="utf-8"></script>
<script>
$(function(){
 $('.bs_join_apply').click(function(){
     var tel = $(this).attr('tel');
    if(confirm('确认登记')){
        $.ajax({
            type: 'post',
            url: "index.php?act=store&op=deal_wx_apply",
            data: {tel: tel},
            dataType: 'json',
            success: function (result) {
                if(result.status==1){
                    location.reload();
                }
            }
        });
    }
 });
});
</script>
