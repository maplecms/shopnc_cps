<?php defined('BYshopJL') or exit('Access Invalid!');?>
<div id="footer" class="footer">
    <div class="footer">
        <div class="footer-top">
            <div class="footer-tleft">
                <a href="<?php echo urlWx('member_index','index');?>" class="btn mr5">个人中心</a>
            </div>
            <a class="gotop" href="javascript:void(0);"><span class="gotop-icon"></span><p>回顶部</p></a>
        </div>
        <div class="footer-content">
            <p class="link">
                <a class="standard" href="/">标准版</a>
                <a href="/download/app/AndroidShopNC2014Moblie.apk">下载Android客户端</a>
            </p>
            <p class="copyright">版权所有 2013-2014 &copy; 网店交流www.shopjl.com</p>
        </div>
    </div>
</div>
<script src="<?php echo SHOP_TEMPLATES_URL; ?>/js/config.js" type="text/javascript"></script>
<script src="<?php echo SHOP_TEMPLATES_URL; ?>/js/zepto.min.js" type="text/javascript"></script>
<script src="<?php echo SHOP_TEMPLATES_URL; ?>/js/touch.js" type="text/javascript"></script>
<script src="<?php echo SHOP_TEMPLATES_URL; ?>/js/common.js" type="text/javascript"></script>
<script src="<?php echo SHOP_TEMPLATES_URL; ?>/js/tmpl/footer.js" type="text/javascript"></script>