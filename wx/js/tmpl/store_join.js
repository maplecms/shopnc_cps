$(function () {
    var memberHtml = '<a class="btn2 mr5" href="' + WapSiteUrl + '/tmpl/member/member.html?act=member">用户中心</a>';
    var tmpl = '<div class="footer">'

        + '<div class="footer-content">'
        + '<p class="link">'
        + '<a href="' + WapSiteUrl + '">首页</a>'
        + '<a href="javascript:location.reload(true);" class="">刷新</a>'
        + '</p>'
            /*+ '<p class="copyright">'
             + '版权所有 2014-2015 © 网店交流www.080c.com'
             + '</p>'*/
        + '</div>'
        + '</div>';
    var render = template.compile(tmpl);
    var html = render();
    $("#footer").html(html);

    var mobile = getcookie('user_mobile');
    $('#usertel').val(mobile);
    var reg = /^1[3-9]\d{9}$/;


    $.sValid.init({
        rules: {
            username: "required",
            usertel: "required",
            company: "required",
            address: "required",
            goods_type: "required"
        },
        messages: {
            username: "姓名必须填写！",
            usertel: "手机号必填!",
            company: "公司名称必填",
            address: "公司地址必填",
            goods_type: "产品类型必填"
        },
        callback: function (eId, eMsg, eRules) {
            if (eId.length > 0) {
                var errorHtml = "";
                $.map(eMsg, function (idx, item) {
                    errorHtml += "<p>" + idx + "</p>";
                });
                $(".error-tips").html(errorHtml).show();
            } else {
                $(".error-tips").html("").hide();
            }
        }
    });
    $('#loginbtn').click(function () {//会员登陆
        var username = $('#username').val();
        var tel = $('#usertel').val();
        var company = $('#company').val();
        var address = $('#address').val();
        var goods_type = $('#goods_type').val();
        var client = 'wap';
        if ($.sValid()) {
            if (!reg.test(tel)) {
                $(".error-tips").html('手机号格式错误').show();
                return false;
            }
            $.ajax({
                type: 'post',
                url: ApiUrl + "/index.php?act=store_join",
                data: {username: username, tel: tel, client: client, company: company, address: address, goods_type: goods_type},
                dataType: 'json',
                success: function (result) {
                    if (!result.datas.error) {
                        $(".error-tips").hide();
                        alert('申请成功，等待回复');
                        wx_redirect(get_root_url()+WapSiteUrl);
                    } else {
                        $(".error-tips").html(result.datas.error).show();
                    }
                }
            });
        }
    });
});