/**
 * Created by beson on 2015/5/7.
 */
/*分享参数*/
var share_title = '';
var share_desc = '';
var share_link = '';
var share_imgUrl = '';
//显示隐藏右侧菜单
function dealOptionMenus() {
    if (getcookie('mobile_bind') == 1) {
        showOptionMenus();
    } else {
        hideOptionMenus();
    }
}


//隐藏菜单
function hideOptionMenus() {
    wx.ready(function () {
        wx.hideOptionMenu();
    });

}
//显示菜单
function showOptionMenus() {
    wx.ready(function () {
        wx.showOptionMenu();
    })
}
//分享统计
function share_count(){
    $.ajax({
        method:'post',
        url:ApiUrl+'?act=member_index&op=share_count'
    });
}

/**
 * 对外分享处理
 */
function share2out() {
    wx.ready(function () {

        //分享到朋友圈
        wx.onMenuShareTimeline({
            title: share_title, // 分享标题
            link: share_link, // 分享链接
            imgUrl: share_imgUrl, // 分享图标
            success: function () {
                // 用户确认分享后执行的回调函数
                share_count();
            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
            }
        });

        //分享给朋友
        wx.onMenuShareAppMessage({
            title: share_title, // 分享标题
            desc: share_desc, // 分享描述
            link: share_link, // 分享链接
            imgUrl: share_imgUrl, // 分享图标
            type: '', // 分享类型,music、video或link，不填默认为link
            dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
            success: function () {
                // 用户确认分享后执行的回调函数
                share_count();
            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
            }
        });
    });
    dealOptionMenus();
}

//判断客户端类型，返回关注url
function browserRedirect() {
    var sUserAgent = navigator.userAgent.toLowerCase();
    var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
    var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
    var bIsMidp = sUserAgent.match(/midp/i) == "midp";
    var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
    var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
    var bIsAndroid = sUserAgent.match(/android/i) == "android";
    var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
    var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";

    if( bIsAndroid || bIsCE || bIsWM){
        var url = "weixin://profile/gh_af7a8a1dab4d";
        return url;
    }else{
        var url = "http://mp.weixin.qq.com/s?__biz=MzAwNDQyODE0Nw==&mid=210730551&idx=1&sn=a4e621cd8581a139727d509969dd44fa#rd";
        return url;
    }
}

//处理关注
function deal_attention(){
    window.location.href = browserRedirect();
}