<?php
defined('BYshopJL') or exit('Access Invalid!');

class wxjs_sdkControl extends wxMemberControl {

    public function __construct(){
        parent::__construct();
    }

    public function indexOp(){
        $wx = new wxSdk();
        $wx->getJsTicket();
        $url = urldecode($_REQUEST['url']);
        $signPackage = $wx->getJsSign($url);
        output_data(array('signPackage' => $signPackage));
    }
}