<?php
/**
 * 店铺首页产品管理
 *
 *
 *
 */
defined('BYshopJL') or exit('Access Invalid!');

class store_wx_indexModel extends Model
{

    public function __construct()
    {
        parent::__construct('bs_store_wx_index');
    }

    /**
     * getStoreIndexGoodsId
     * 获取首页产品id
     * @param $condition
     * @return bool|mixed
     */
    public function getStoreIndexGoodsId($condition)
    {
        $result = $this->where($condition)->field('goods_id_list')->find();
        if ($result['goods_id_list']) return unserialize($result['goods_id_list']);
        return false;
    }

    /**
     * getStoreIndexGoodsCommonId
     * 获取首页产品goods_commonid
     * @param $condition
     * @return array|bool
     */
    public function getStoreIndexGoodsCommonId($condition){
        $data = $this->getStoreIndexGoodsId($condition);
        if($data){
            $map = array();
            $str_goods_id = implode(',',$data);
            $map['goods_id'] = array('in',"$str_goods_id");
            $result = Model()->table('goods')->field('goods_commonid')->distinct(true)->select();
            $return  = array();
            if($result){
                foreach($result as $v){
                    array_push($return,$v);
                }
                return $return;
            }
            return false;
        }
        return false;
    }


    /**
     * updateIndex
     * 更新信息
     * @param $condition
     * @param $data
     * @return bool
     */
    public function updateIndex($condition,$data){
        $rs = $this->where($condition)->update($data);
        if($rs) return true;
        return false;
    }

    /**
     * addIndex
     * 插入数据
     * @param $data
     * @return bool
     */
    public function addIndex($data){
        $rs = $this->insert($data);
        if($rs) return true;
        return false;
    }
}
