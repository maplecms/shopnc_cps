<?php
/**
 * Created by PhpStorm.
 * User: BesonTD
 * Date: 2015/3/28
 * Time: 9:53
 */
defined('BYshopJL') or exit('Access Invalid!');
require_once(BASE_DATA_PATH.'/model/mb_user_token.model.php');
class wx_mb_user_tokenModel extends mb_user_tokenModel{
    public function __construct(){
        parent::__construct();
    }

    public function getMbUserTokenInfo($condition,$field='*',$order='token_id desc') {
        return $this->where($condition)->field($field)->order($order)->find();
    }
}