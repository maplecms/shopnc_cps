<?php

defined('BYshopJL') or exit('Access Invalid!');

class goods_member_share_viewModel extends Model
{
    public function __construct()
    {
        parent::__construct('bs_member_share_view');
    }

    /**
     * updateGoodsShareView
     * 更新分享浏览记录
     * @param $goods_id
     * @return bool
     */
    public function updateMemberShareView($goods_id)
    {
        if (empty($goods_id) || empty($_GET['rec'])) return false;
        $rec = trim($_GET['rec']);
        $member_model = Model('member');
        $goods_model = Model('goods');

        //查询用户信息
        $member_info = $member_model->infoMember(array('share_sign' => $rec));
        if (!$member_info) return false;
        $member_id = $member_info['member_id'];

        //判断商品正确性
        $count = $goods_model->getGoodsCount(array('goods_id' => $goods_id));
        if ($count == 0) return false;


        $year_month = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $view_count = $this->query("SELECT COUNT('member_id') FROM {$this->table_prefix}bs_member_share_view WHERE  `year_month`=$year_month AND `member_id`=$member_id ");
        if (intval($view_count[0]["COUNT('member_id')"]) == 0) {
            $sql = "INSERT INTO {$this->table_prefix}bs_member_share_view  (`member_id`,`year_month`,`month_view`,`d" . date('d') . "`) VALUES($member_id,$year_month,1,1)";
        } else {
            $sql = "UPDATE {$this->table_prefix}bs_member_share_view SET `d" . date('d') . "`= `d" . date('d') . "`+1 ,`month_view` = `month_view`+1 WHERE `year_month`=$year_month AND  `member_id`=$member_id";
        }
        $flag = $this->query($sql);
        return true;
    }
}
