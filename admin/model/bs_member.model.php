<?php
/**
 * 会员模型
 *
 *
 *
 *
 * by www.besonit.com
 */
defined('BYshopJL') or exit('Access Invalid!');
require_once(BASE_DATA_PATH.'/model/member.model.php');
class bs_memberModel extends memberModel {

    public function __construct(){
        parent::__construct('member');
    }

    /**
     * 会员详细信息（查库）
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getMemberInfo($condition, $field = '*', $master = false) {
        return $this->table('member,bs_brokerage')->field($field)->join('left')->on('member.member_id = bs_brokerage.bk_member_id')->where($condition)->master($master)->find();
    }

	/**
	 * 会员列表
	 * @param array $condition
	 * @param string $field
	 * @param number $page
	 * @param string $order
	 */
	public function getMemberList($condition = array(), $field = '*', $page = 0, $order = 'member_id desc', $limit = '') {
		return $this->table('member,bs_brokerage')->where($condition)->join('left')->on('member.member_id = bs_brokerage.bk_member_id')->page($page)->order($order)->limit($limit)->select();
	}
}
