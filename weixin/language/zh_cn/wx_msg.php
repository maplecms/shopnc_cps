<?php
defined('BYshopJL') or exit('Access Invalid!');

$lang['wx_order_new'] = '新订单提示';
$lang['wx_order_first_send_buyer'] = '您的订单已经标记发货，请留意查收。';//发货通知，发个买家
$lang['wx_order_first_send_buyer_offline'] = '亲，您购买的商品已经发货成功了!请注意查收。祝您生活愉快';//发货通知，（货到付款）发个买家
$lang['wx_order_first_send_seller'] = '您刚刚执行订单发货操作，订单信息如下：';//发货通知，发个卖家
$lang['wx_order_first_send_cash_back'] = '您的朋友通过您的分享链接购买成功产品，【微享购】 已将您的返现金额打入到您的账户中，欢迎继续购物分享：';//返现通知


$lang['wx_order_state_new'] = '待付款';
$lang['wx_order_state_pay'] = '待发货';
$lang['wx_order_state_payed'] = '已付款';
$lang['wx_order_state_send'] = '待收货';
$lang['wx_order_state_success'] = '交易完成';
$lang['wx_order_state_eval'] = '已评价';
$lang['wx_order_state_pay_way_online'] = '在线支付';
$lang['wx_order_state_pay_way_offline'] = '货到付款';

$lang['wx_order_buyer_remark_new'] = '祝您生活愉快！';//买家新订单remark提示
$lang['wx_order_seller_remark_new'] = '您的商店产生一条新订单，请及时处理--【微享购】';//卖家新订单remark提示
$lang['wx_order_buyer_remark_offline'] = '如果您还有任何疑问，请及时联系商家：';//买家货到付款，发货remark提示
$lang['wx_order_buyer_remark_online'] = '如果您还有任何疑问，请及时联系商家：';//买家在线支付，发货remark提示
$lang['wx_order_seller_remark_send'] = '【微享购】祝您生意兴隆。';//卖家，发货remark提示
$lang['wx_order_seller_remark_cash_back'] = '【微享购】祝您生活愉快！';//卖家，发货remark提示



