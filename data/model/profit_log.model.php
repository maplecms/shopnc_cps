<?php
/**
 * Created by PhpStorm.
 * User: BesonTD
 * Date: 2015/3/28
 * Time: 9:53
 */
defined('BYshopJL') or exit('Access Invalid!');
//require_once(BASE_DATA_PATH.'/model/model.model.php');
class profit_logModel extends Model{
    public function __construct(){
        parent::__construct('bs_profit_log');
    }

    public function getProfitList($condition, $order='bs_profit_log.profit_log_id desc',$page=10){
        if($_GET['statistics']){
            $data_list = $this->table('bs_profit_log,order,member')
                ->field('bs_profit_log.*,order.order_sn,order.finnshed_time,order.order_amount,member.member_mobile,sum(bs_profit_log.amount) as all_amount,count(bs_profit_log.order_id) as all_order')
                ->join('left')->on('bs_profit_log.order_id = order.order_id ,bs_profit_log.member_id = member.member_id')
                ->where($condition)->order($order)->page($page)->group('bs_profit_log.member_id')->select();
        }else{
            $data_list = $this->table('bs_profit_log,order,member')
                ->field('bs_profit_log.*,order.order_sn,order.finnshed_time,order.order_amount,member.member_mobile')
                ->join('left')->on('bs_profit_log.order_id = order.order_id ,bs_profit_log.member_id = member.member_id')
                ->where($condition)->order($order)->page($page)->select();
        }

        if (empty($data_list)) return array();
        return $data_list;
    }

}