<?php
/**
 * Created by PhpStorm.
 * User: BesonTD
 * Date: 2015/3/28
 * Time: 9:53
 */
defined('BYshopJL') or exit('Access Invalid!');
require_once(BASE_DATA_PATH.'/model/message.model.php');
class wx_messageModel extends messageModel{

    public function saveMessage($param) {
        if($param['member_id'] == '') {
            return false;
        }
        $array	= array();
        $array['message_parent_id'] = $param['message_parent_id']?$param['message_parent_id']:'0';
        $array['from_member_id']	= $param['from_member_id'] ? $param['from_member_id'] : '0' ;
        $array['from_member_name']	= $param['from_member_name'] ? $param['from_member_name'] : '' ;
        $array['to_member_id']	    = $param['member_id'];
        $array['to_member_name']	= $param['to_member_name']?$param['to_member_name']:'';
        $array['message_body']		= trim($param['msg_content']);
        $array['message_title']		= trim($param['msg_title']);
        $array['message_time']		= time();
        $array['message_update_time']= time();
        $array['message_type']		= $param['message_type']?$param['message_type']:'0';
        $array['message_ismore']	= $param['message_ismore']?$param['message_ismore']:'0';
        $array['read_member_id']	= $param['read_member_id']?$param['read_member_id']:'';
        $array['del_member_id']	= $param['del_member_id']?$param['del_member_id']:'';
        return Db::insert('message',$array);
    }
}