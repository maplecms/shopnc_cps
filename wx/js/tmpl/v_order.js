$(function(){
	var key = getcookie('key');
    var buyer_id = GetQueryString('buyer_id');
	if(key==''){
		logout(window.location.href);
	}
    var pay_sn = GetQueryString('pay_sn');

    var readytopay = false;

		$.ajax({
			type:'post',
			url:ApiUrl+"/index.php?act=member_vr_order&op=order_info&buyer_id="+buyer_id,
			data:{key:key,pay_sn:pay_sn},
			dataType:'json',
			success:function(result){
				checklogin(result.login);//检测是否登录了
				var data = result.datas;
				data.key = getcookie('key');
				template.helper('$getLocalTime', function (nS) {
                    var d = new Date(parseInt(nS) * 1000);
                    var s = '';
                    s += d.getFullYear() + '年';
                    s += (d.getMonth() + 1) + '月';
                    s += d.getDate() + '日 ';
                    s += d.getHours() + ':';
                    s += d.getMinutes();
                    return s;
				});
                template.helper('p2f', function(s) {
                    return (parseFloat(s) || 0).toFixed(2);
                });
				var html = template.render('order-list-tmpl', data);
				$("#order-list").html(html);


                function callpay(){

                    //wx.chooseWXPay({
                    //    appId: data.order_info.pay_param.appId,
                    //    timestamp: data.order_info.pay_param.timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
                    //    nonceStr: data.order_info.pay_param.nonceStr, // 支付签名随机串，不长于 32 位
                    //    package: data.order_info.pay_param.package, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
                    //    signType: data.order_info.pay_param.signType, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
                    //    paySign: data.order_info.pay_param.paySign, // 支付签名
                    //    success: function (res) {
                    //        // 支付成功后的回调函数
                    //    }
                    //});

                    WeixinJSBridge.invoke(
                        'getBrandWCPayRequest', {
                            "appId" : data.order_info.pay_param.appId,     //公众号名称，由商户传入
                            "timeStamp": data.order_info.pay_param.timeStamp, //时间戳，自1970年以来的秒数
                            "nonceStr" : data.order_info.pay_param.nonceStr, //随机串
                            "package" : data.order_info.pay_param.package,
                            "signType" : data.order_info.pay_param.signType,         //微信签名方式:
                            "paySign" : data.order_info.pay_param.paySign //微信签名
                        },
                        function(res){
                            if(res.err_msg == "get_brand_wcpay_request:ok" ) {
                                wx_redirect(WapSiteUrl+'/tmpl/member/vr_order_list.html');
                            }     // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回ok，但并不保证它绝对可靠。
                        }
                    );

                }





                $('.check-payment').click(function() {
                    if (!readytopay) {
                        $.sDialog({
                            skin:"red",
                            content:'暂无可用的支付方式',
                            okBtn:false,
                            cancelBtn:false
                        });
                        return false;
                    }
                    callpay();
                });
                $(window).scrollTop(0);
                bind4ChangHref();
                $.each($('.order-shop-total .mt5 span'),function(i,obj){
                    $(obj).html($(obj).text());
                });
			}
		});

        $.ajax({
            type:'get',
            url:ApiUrl+"/index.php?act=member_payment&op=payment_list",
            data:{key:key},
            dataType:'json',
            success:function(result){
                $.each((result && result.datas && result.datas.payment_list) || [], function(k, v) {
                    // console.log(v);
                    if (v == 'alipay') {
                        readytopay = true;
                        return false;
                    }
                });
            }
        });



});
